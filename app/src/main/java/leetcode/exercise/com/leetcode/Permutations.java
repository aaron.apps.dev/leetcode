package leetcode.exercise.com.leetcode;


/**
 * Time Complexity: O(n!)
 */
public class Permutations {

    public static void main(String[] args) {
        char[] chars = new char[]{'1', '2', '3'};
        perm(chars, 0, chars.length - 1);
        // Expected result: 123, 132, 213, 231, 312, 321
    }

    private static void perm(char[] str, int from, int to) {
        if (from == to) {
            System.out.println(str);
            return;
        }

        for (int i = from; i <= to; i++) {
            swap(str, from, i); // 將第 i 個字換到第一個
            perm(str, from + 1, to); // 除了第一個字之外，對剩下的字作排列
            swap(str, i, from); // 將第一個字換回原本 i 的位置
        }
    }

    private static void swap(char[] str, int i, int j) {
        char tmp = str[i];
        str[i] = str[j];
        str[j] = tmp;
    }

}
