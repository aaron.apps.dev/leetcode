package leetcode.exercise.com.leetcode;

import java.util.HashSet;
import java.util.Set;

class LongestSubstringWithoutRepeatingCharacters {

    public static void main(String[] args) {
        LongestSubstringWithoutRepeatingCharacters instance = new LongestSubstringWithoutRepeatingCharacters();
        System.out.println(instance.lengthOfLongestSubstring("pwwkew")); // 3
        System.out.println(instance.lengthOfLongestSubstring("abcabcbb")); // 3
        System.out.println(instance.lengthOfLongestSubstring("bbbbb")); // 1
        System.out.println(instance.lengthOfLongestSubstring("abcabcd")); // 4
        System.out.println(instance.lengthOfLongestSubstring("aabbccdd")); // 2
        System.out.println(instance.lengthOfLongestSubstring("dvdf")); // 3
    }

    private int lengthOfLongestSubstring(String s) {
        int startIndex = 0;
        int longestLength = 0;
        Set<Character> hashSet = new HashSet<>();
        for (int index = startIndex; index < s.length(); ) {
            Character character = s.charAt(index);
            if (hashSet.contains(character)) {
                longestLength = Math.max(hashSet.size(), longestLength);
                index = s.indexOf(character, startIndex) + 1;
                startIndex = index;
                hashSet.clear();
                continue;
            }
            hashSet.add(character);
            index++;
        }
        return Math.max(hashSet.size(), longestLength);
    }

}