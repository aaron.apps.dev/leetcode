package leetcode.exercise.com.leetcode;

public class LongestCommonPrefix {

    public static void main(String[] args) {
        String[] strs = new String[]{
                "abc",
                "abcbc",
                "abcd",
                "abce"
        };
        LongestCommonPrefix instance = new LongestCommonPrefix();
        System.out.println(instance.longestCommonPrefix(strs));
    }

    public String longestCommonPrefix(String[] strs) {
        if (strs.length == 0) {
            return "";
        } else if (strs.length == 1) {
            return strs[0];
        } else {
            int minLength = Integer.MAX_VALUE;
            for (String str : strs) {
                minLength = Math.min(minLength, str.length());
            }

            int charIndex = 0;
            while (charIndex < minLength) {
                char comparedChar = strs[0].charAt(charIndex);
                for (int i = 1; i < strs.length; i++) {
                    if (strs[i].charAt(charIndex) != comparedChar) {
                        return strs[0].substring(0, charIndex);
                    }
                }
                charIndex++;
            }
            return strs[0].substring(0, charIndex);
        }
    }

}
