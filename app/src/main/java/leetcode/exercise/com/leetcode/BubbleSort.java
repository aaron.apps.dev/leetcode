package leetcode.exercise.com.leetcode;

public class BubbleSort {

    public static void main(String[] args) {
        int[] values = new int[]{3, 2, 3, 5, 2, 4, 1, 9, 8};
        print(values);
        sort(values);
        print(values);
    }

    private static void print(int[] values) {
        for (int i = 0; i < values.length; i++) {
            System.out.print(values[i] + " ");
        }
        System.out.println();
    }

    private static void sort(int[] values) {
        for (int i = 0; i < values.length - 1; i++) {
            for (int j = 0; j < values.length - 1 - i; j++) {
                if (values[j] > values[j + 1]) {
                    swap(values, j, j + 1);
                }
            }
        }
    }

    private static void swap(int[] values, int index1, int index2) {
        int tmp = values[index1];
        values[index1] = values[index2];
        values[index2] = tmp;
    }

}
