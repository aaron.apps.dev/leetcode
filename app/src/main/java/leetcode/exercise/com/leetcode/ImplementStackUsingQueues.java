package leetcode.exercise.com.leetcode;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ImplementStackUsingQueues {

    public static void main(String[] args) {
        MyStack stack = new MyStack();
        System.out.println(stack.empty());
        stack.push(1);
        System.out.println(stack.empty());
        System.out.println(stack.top());
        stack.push(2);
        System.out.println(stack.top());
        stack.push(3);
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.empty());
    }

    static class MyStack {

        private List<Integer> mQueue;

        /**
         * Initialize your data structure here.
         */
        public MyStack() {
            mQueue = new LinkedList<>();
        }

        /**
         * Push element x onto stack.
         */
        public void push(int x) {
            mQueue.add(x);
        }

        /**
         * Removes the element on top of the stack and returns that element.
         */
        public int pop() {
           return mQueue.remove(mQueue.size() - 1);
        }

        /**
         * Get the top element.
         */
        public int top() {
            return mQueue.get(mQueue.size() - 1);
        }

        /**
         * Returns whether the stack is empty.
         */
        public boolean empty() {
            return mQueue.isEmpty();
        }
    }

}
