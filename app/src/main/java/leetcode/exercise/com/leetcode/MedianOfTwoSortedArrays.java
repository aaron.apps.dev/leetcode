package leetcode.exercise.com.leetcode;


public class MedianOfTwoSortedArrays {

    public static void main(String[] args) {
        MedianOfTwoSortedArrays instance = new MedianOfTwoSortedArrays();
        System.out.println(instance.findMedianSortedArrays(new int[]{1, 2}, new int[]{2, 3})); // 2
        System.out.println(instance.findMedianSortedArrays(new int[]{1}, new int[]{2, 3})); // 2
        System.out.println(instance.findMedianSortedArrays(new int[]{1, 2, 3}, new int[]{2, 3, 4})); // 2.5
        System.out.println(instance.findMedianSortedArrays(new int[]{1, 2}, new int[]{1, 2})); // 1.5
        System.out.println(instance.findMedianSortedArrays(new int[]{1}, new int[]{1})); // 1
    }

    private double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int i = 0, j = 0;
        int[] mergeNums = new int[nums1.length + nums2.length];
        for (int index = 0; index < mergeNums.length; index++) {
            if (i < nums1.length && j < nums2.length) {
                if (nums1[i] <= nums2[j]) {
                    mergeNums[index] = nums1[i++];
                } else {
                    mergeNums[index] = nums2[j++];
                }
            } else if (i < nums1.length) {
                mergeNums[index] = nums1[i++];
            } else if (j < nums2.length) {
                mergeNums[index] = nums2[j++];
            }
        }

        int medianIndex = mergeNums.length / 2;
        if (mergeNums.length % 2 == 0) {
            return (mergeNums[medianIndex - 1] + mergeNums[medianIndex]) / 2f;
        } else {
            return mergeNums[medianIndex];
        }
    }

}
