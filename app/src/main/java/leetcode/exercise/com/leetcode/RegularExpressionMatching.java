package leetcode.exercise.com.leetcode;

/**
 * Refer to <a href="https://leetcode.com/problems/regular-expression-matching/description/">
 * https://leetcode.com/problems/regular-expression-matching/description/</a>.
 */
public class RegularExpressionMatching {

    public static void main(String[] args) {
        RegularExpressionMatching instance = new RegularExpressionMatching();
        System.out.println(instance.isMatch("aabcd", "a*.bc*.*")); // true
    }

    public boolean isMatch(String text, String regex) {
        if (regex.isEmpty()) return text.isEmpty();
        boolean firstMatch = !text.isEmpty() && (regex.charAt(0) == '.' || text.charAt(0) == regex.charAt(0));
        if (regex.length() >= 2 && regex.charAt(1) == '*') {
            return isMatch(text, regex.substring(2)) || (firstMatch && isMatch(text.substring(1), regex));
        } else {
            return firstMatch && isMatch(text.substring(1), regex.substring(1));
        }
    }

}
