package leetcode.exercise.com.leetcode;

public class BinarySearch {

    public static void main(String[] args) {
        int[] list = new int[]{1, 3, 6, 7, 9, 143, 245, 400};
        System.out.println(binarySearch(list, 0, list.length - 1, 0));
        System.out.println(binarySearch(list, 0, list.length - 1, 1));
        System.out.println(binarySearch(list, 0, list.length - 1, 3));
        System.out.println(binarySearch(list, 0, list.length - 1, 6));
        System.out.println(binarySearch(list, 0, list.length - 1, 7));
        System.out.println(binarySearch(list, 0, list.length - 1, 9));
        System.out.println(binarySearch(list, 0, list.length - 1, 143));
        System.out.println(binarySearch(list, 0, list.length - 1, 245));
        System.out.println(binarySearch(list, 0, list.length - 1, 400));
        System.out.println(binarySearch(list, 0, list.length - 1, 404));
    }

    private static int binarySearch(int[] list, int fromIndex, int toIndex, int key) {
        int low = fromIndex;
        int high = toIndex;
        while (low <= high) {
            int mid = (low + high) / 2;
            if (list[mid] > key) {
                high = mid - 1;
            } else if (list[mid] < key) {
                low = mid + 1;
            } else {
                return mid;
            }
        }
        return -1;
    }

}
