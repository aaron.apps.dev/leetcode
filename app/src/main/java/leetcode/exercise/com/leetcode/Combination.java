package leetcode.exercise.com.leetcode;

public class Combination {

    public static void main(String[] args) {
        char[] chars = new char[]{'1', '2', '3'};
        StringBuilder sb = new StringBuilder();
        combina(chars, sb, 0); // 2*2*2*...*2 = 2^n 種組合
    }

    private static void combina(char[] src, StringBuilder concatStr, int pos) {
        if (pos == src.length) {
            System.out.println(concatStr);
            return;
        }

        // 不選擇現在 pos 的 char，用 concatStr 去串下一個 char (可選/可不選)
        combina(src, concatStr, pos + 1);
        // 選擇現在 pos 的 char，並把它串在 concatStr 之後，再用串完的字串去串下一個 char (可選/可不選)
        combina(src, concatStr.append(src[pos]), pos + 1);
        concatStr.delete(concatStr.length() - 1, concatStr.length());
    }

}
