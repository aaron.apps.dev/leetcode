package leetcode.exercise.com.leetcode;

public class LongestPalindromicSubstring {

    public static void main(String[] args) {
        LongestPalindromicSubstring instance = new LongestPalindromicSubstring();
        System.out.println(instance.longestPalindrome("abcdcb")); // bcdcb
        System.out.println(instance.longestPalindrome("babad")); // bab
        System.out.println(instance.longestPalindrome("cbbd")); // bb
        System.out.println(instance.longestPalindrome("aaabaaaa")); // aaabaaa
        System.out.println(instance.longestPalindrome("aaa")); // aaa
        System.out.println(instance.longestPalindrome("a")); // a
    }

    /**
     * Dynamic programming. Refers to
     * <a href="https://leetcode.com/problems/longest-palindromic-substring/solution/#approach-3-dynamic-programming-accepted">Approach 3</a>.
     */
    private String longestPalindrome(String s) {
            int maxStartIndex = 0, maxEndIndex = 0;
            boolean[][] isPalindrome = new boolean[s.length()][s.length()];
            for (int len = 1; len <= s.length(); len++) {
                for (int startIndex = 0; startIndex <= s.length() - len; startIndex++) {
                    int endIndex = startIndex + len - 1;
                    if (len == 1) {
                        isPalindrome[startIndex][endIndex] = true;
                    } else if (s.charAt(startIndex) == s.charAt(endIndex)) { // len >= 2 && startChar == endChar
                        isPalindrome[startIndex][endIndex] = len == 2 || isPalindrome[startIndex + 1][endIndex - 1];
                    } else {
                        isPalindrome[startIndex][endIndex] = false;
                    }

                    if (isPalindrome[startIndex][endIndex] && len > (maxEndIndex - maxStartIndex + 1)) {
                        maxStartIndex = startIndex;
                        maxEndIndex = endIndex;
                    }
                }
            }
            return s.substring(maxStartIndex, maxEndIndex + 1);
    }

}
