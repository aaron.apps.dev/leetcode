package leetcode.exercise.com.leetcode;

public class QuickSort {

    public static void main(String[] args) {
        int[] array = new int[]{3, 5, 8, 9, 4, 7, 2, 6};
        quickSort(array, 0, array.length - 1);
        print(array);
    }

    private static void quickSort(int[] array, int left, int right) {
        if (left >= right) {
            return;
        }
        int pivot = array[(left + right) / 2];
        int index = partition(array, left, right, pivot);
        quickSort(array, left, index - 1);
        quickSort(array, index, right);
    }

    /**
     * @return the index of pivot.
     */
    private static int partition(int[] array, int left, int right, int pivot) {
        while (left <= right) {
            while (array[left] < pivot) left++;
            while (array[right] > pivot) right--;
            if (left <= right) {
                swap(array, left, right);
                left++;
                right--;
            }
        }
        return left;
    }

    private static void swap(int[] array, int i, int j) {
        int tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
    }

    private static void print(int[] array) {
        for (int value : array) {
            System.out.print(value + " ");
        }
    }
}
