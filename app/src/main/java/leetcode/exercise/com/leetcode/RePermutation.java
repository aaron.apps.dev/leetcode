package leetcode.exercise.com.leetcode;

/**
 * 可重複排列 (放回全排列)
 * 123三個字母組合的所有長度為3的字符串，aaa，aab，aac ...... ccc一共27種
 */
public class RePermutation {

    public static void main(String[] args) {
        char[] chars = new char[]{'a', 'b', 'c'};
        StringBuilder sb = new StringBuilder();
        rePerm(chars, sb);
    }

    private static void rePerm(char[] src, StringBuilder concatStr) {
        if (concatStr.length() == src.length) {
            System.out.println(concatStr.toString());
            return;
        }

        // 每次有 n 種選擇
        for (int i = 0; i < src.length; i++) {
            rePerm(src, concatStr.append(src[i])); // 依序選擇一個字 append 繼續遞迴
            concatStr.delete(concatStr.length() - 1, concatStr.length()); // 將上一步 append 的字刪除，選擇 append 下一個字。
        }
    }

}
