package leetcode.exercise.com.leetcode;


import java.util.Stack;

public class ImplementQueueUsingStacks {

    public static void main(String[] args) {
        MyQueue queue = new MyQueue();
        System.out.println(queue.empty());
        queue.push(1);
        System.out.println(queue.empty());
        System.out.println(queue.peek());
        queue.push(2);
        queue.push(3);
        System.out.println(queue.pop());
        System.out.println(queue.pop());
        System.out.println(queue.pop());
    }

    static class MyQueue {

        private Stack<Integer> mPopStack;
        private Stack<Integer> mPushStack;

        /**
         * Initialize your data structure here.
         */
        public MyQueue() {
            mPopStack = new Stack<>();
            mPushStack = new Stack<>();
        }

        /**
         * Push element x to the back of queue.
         */
        public void push(int x) {
            mPushStack.push(x);
        }

        /**
         * Removes the element from in front of queue and returns that element.
         */
        public int pop() {
            if (mPopStack.empty()) {
                while (!mPushStack.empty()) {
                    mPopStack.push(mPushStack.pop());
                }
            }
            return mPopStack.pop();
        }

        /**
         * Get the front element.
         */
        public int peek() {
            if (mPopStack.empty()) {
                while (!mPushStack.empty()) {
                    mPopStack.push(mPushStack.pop());
                }
            }
            return mPopStack.peek();
        }

        /**
         * Returns whether the queue is empty.
         */
        public boolean empty() {
            return mPushStack.empty() && mPopStack.empty();
        }
    }

}
