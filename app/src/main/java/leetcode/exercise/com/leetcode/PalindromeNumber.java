package leetcode.exercise.com.leetcode;

public class PalindromeNumber {

    public static void main(String[] args) {
        PalindromeNumber palindromeNumber = new PalindromeNumber();
        System.out.println("1: " + palindromeNumber.isPalindrome(1));
        System.out.println("11: " + palindromeNumber.isPalindrome(11));
        System.out.println("12: " + palindromeNumber.isPalindrome(12));
        System.out.println("123: " + palindromeNumber.isPalindrome(123));
        System.out.println("1232: " + palindromeNumber.isPalindrome(1232));
        System.out.println("12321: " + palindromeNumber.isPalindrome(12321));
        System.out.println("123321: " + palindromeNumber.isPalindrome(123321));
    }

    private boolean isPalindrome(int number) {
        String numText = String.valueOf(number);
        int startIndex = 0;
        int endIndex = numText.length() - 1;
        while (startIndex <= endIndex) {
            if (numText.charAt(startIndex) != numText.charAt(endIndex)) {
                break;
            }
            startIndex++;
            endIndex--;
        }
        return startIndex > endIndex;
    }

}
